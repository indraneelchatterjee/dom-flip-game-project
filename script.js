//////// Variables

let startButton = document.getElementById("start-btn");
let timerDisplay = document.getElementsByClassName("timer");
let movesCount = document.getElementsByClassName("num-moves");
let count = 0;
let flippedCards = [];
let timer;
let seconds = 0;
let istimerRunning = false;
let imageName = [
  "apple",
  "apple",
  "book",
  "book",
  "carrot",
  "carrot",
  "orangeslice",
  "orangeslice",
  "orange",
  "orange",
  "pineapple",
  "pineapple",
  "melon",
  "melon",
  "cabbage",
  "cabbage",
];
let pairs = imageName.length / 2;

//     Shuffling the array to get different arrangement everytime
function shuffleArray(imageName) {
  for (let index = 0; index < imageName.length; index++) {
    let j = Math.floor(Math.random() * imageName.length);
    let temp = imageName[index];
    imageName[index] = imageName[j];
    imageName[j] = temp;
  }
  return imageName;
}
let shuffledArray = shuffleArray(imageName);

//     Creating elements
function createGameLayout(shuffledArray) {
  var container = document.querySelector(".cards-container");
  for (var i = 0; i < shuffledArray.length; i++) {
    let div = document.createElement("div");
    div.classList.add("img-card", "hide-cards");
    div.setAttribute("id", `${shuffledArray[i]}`);
    let image = document.createElement("img");
    image.src = `./resources/${shuffledArray[i]}.png`;
    image.classList.add("img-back");
    div.appendChild(image);
    container.appendChild(div);
  }
}
createGameLayout(shuffledArray);

//     Count down logic
function startTimer() {
  if (istimerRunning === false) {
    timerStart();
    revealAllCards();
    istimerRunning = true;
    startButton.removeEventListener("click", startTimer);
    setTimeout(() => {
      startButton.addEventListener("click", startTimer);
      startButton.innerText = "Stop";
    }, 2000);
    startB;
  } else {
    hideAllCards();
    removeListenersFromCards();
    startButton.innerText = "Start";
    clearInterval(timer);
    timer = null;
    seconds = 0;
    count = 0;
    istimerRunning = false;
    timerDisplay[0].innerText = "time:" + seconds + " sec";
    movesCount[0].innerText = count + ":moves";
  }
}
function timerStart() {
  setTimeout(() => {
    timer = setInterval(updateTimer, 1000);
  }, 2000);
}
function updateTimer() {
  seconds++;
  timerDisplay[0].innerText = "time:" + seconds + " sec";
}
startButton.addEventListener("click", startTimer);

//      Removing Listner from the cards.
function removeListenersFromCards() {
  cards.forEach((card) => {
    card.removeEventListener("click", hideCards);
  });
}
//     Function to show all cards
function revealAllCards() {
  cards.forEach((card) => {
    card.classList.remove("hide-cards", "matched");
    removeListenersFromCards();
  });

  setTimeout(hideAllCards, 2000);
}

//     Function to hide all cards
function hideAllCards() {
  cards.forEach((card) => {
    card.classList.add("hide-cards");
    card.addEventListener("click", hideCards);
  });
}

//     Counting & pair matching logic
let cards = document.querySelectorAll(".img-card");
function hideCards() {
  if (!this.classList.contains("matched")) {
    this.classList.toggle("hide-cards");
    this.removeEventListener("click", hideCards);
    count++;
    movesCount[0].innerText = count + ":moves";
  }

  if (flippedCards.length >= 2 || this.classList.contains("matched")) {
    return;
  }

  if (this.classList.contains("hide-cards")) {
    flippedCards = flippedCards.filter((card) => card !== this);
  } else {
    flippedCards.push(this);
  }

  if (flippedCards.length === 2) {
    flippedCards.forEach((card) => card.addEventListener("click", hideCards));

    if (flippedCards[0].id === flippedCards[1].id) {
      flippedCards[0].classList.add("matched");
      flippedCards[1].classList.add("matched");
      flippedCards = [];
      pairs--;
    } else {
      setTimeout(function () {
        flippedCards[0].classList.add("hide-cards");
        flippedCards[1].classList.add("hide-cards");
        flippedCards = [];
      }, 400);
    }

    if (pairs === 0) {
      clearInterval(timer);
      startButton.disabled = false;
      let result = document.querySelector(".game-ended");
      result.style.display = "block";
      document
        .querySelector(".reload-btn")
        .addEventListener("click", function () {
          location.reload();
        });
      console.log("Game Over");
    }
  }
}
